/*
 * NAME:    Adam Lindsey
 * LOGIN:   alindsey
 * TITLE:   CIS415 Project 0
 * FILE:    mentry.c
 * --This is my own personal work--
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "mentry.h"

#define N 1024

/* me_get returns the next file entry, or NULL if end of file*/
MEntry *me_get(FILE *fd){
   int i=0,j=0;
   MEntry *me;
	char l1[N] = "", l2[N] = "", l3[N] = "";
   
	if((me = (MEntry *)calloc(1,sizeof(MEntry))) != NULL)
   {
      me->surname = calloc(1,sizeof(char)*N);
      me->zipcode = calloc(1,sizeof(char)*N);
      me->full_address = calloc(3,sizeof(char)*N);
		if((me->surname) != NULL) 
      {
         fgets(l1,N,fd);
         if(l1[0]=='\0')
         {
   			me_destroy(me);
            return NULL;
   		}
         for(i=0;l1[i]!=',';i++) me->surname[i] = tolower(l1[i]);
   		me->surname[i] = '\0';
      } else {
         me_destroy(me);
         return NULL;
      }
      
      if((me->zipcode) != NULL) 
      {
         fgets(l2,N,fd);
         me->house_number = atoi(l2);
      } else {
         me_destroy(me);
         return NULL;
      }
      
      if((me->full_address) != NULL) 
      {
         fgets(l3,N,fd);
         sprintf(me->full_address,"%s%s%s",l1,l2,l3);
         for(i=0,j=0;l3[i]!='\n';i++)
         {
   			if(isalnum(l3[i]) && l3[i]!=' ')
            {
   				me->zipcode[j]=l3[i];
   				j++;
   			}
   		}
   		me->zipcode[j] = '\0';
      } else {
         me_destroy(me);
         return NULL;
      }
	} else {
      free(me);
      return NULL;
   }
	return me; 
}

/* me_hash computes a hash of the MEntry, mod size */
unsigned long me_hash(MEntry *me, unsigned long size)
{
	unsigned long val = 0, val_temp = 0;
	int i = 0;
	for(i=0,val_temp=0;me->surname[val_temp]!='\0';i++) val_temp += (int) me->surname[i];
	val += val_temp;
	for(i=0,val_temp=0;me->zipcode[val_temp]!='\0';i++) val_temp += (int) me->zipcode[i];
	val += val_temp;
	val += me->house_number;
	return (val%size);
}

/* me_print prints the full address on fd */
void me_print(MEntry *me, FILE *fd)
{
	fprintf(fd,"%s",me->full_address);
}

/* me_compare compares two mail entries, returning <0, 0, >0 if
 * me1<me2, me1==me2, me1>me2 */
int me_compare(MEntry *me1, MEntry *me2)
{
   int var = 0;
	if((var = strcmp(me1->surname,me2->surname))!=0) 
   {
      return var;
   } 
   else if((var = strcmp(me1->zipcode,me2->zipcode))!=0) 
   {
      return var;
   } 
   else if(me1->house_number != me2->house_number) 
   {
		return (me1->house_number < me2->house_number ? 1 : -1);
   } 
   else return var;
}

/* me_destroy destroys the mail entry*/
void me_destroy(MEntry *me)
{
	free(me->surname);
   free(me->zipcode);
	free(me->full_address);
	free(me);
}


