/*
 * NAME:    Adam Lindsey
 * LOGIN:   alindsey
 * TITLE:   CIS415 Project 0
 * FILE:    mlist.c
 * --This is my own personal work
 *    but was derived from the provided file--
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "mlist.h"
#include "mentry.h"

typedef struct mnode 
{
	struct mnode *next;
	MEntry *me;
} 
MNode;

typedef struct mlist 
{
	int list_size;
	MNode **nodes;
} 
MList;

/* ml_create - created a new mailing list */
MList *ml_create(void) 
{
   MList *p;
	int n = 0;   
	if((p = (MList *) malloc(sizeof(MList)))==NULL)
   {
		free(p);
      return NULL;
	}
	if( (p->nodes = (MNode **) calloc(20,sizeof(MNode *)))!=NULL)
   {
		for(n=0;n<20;n++)
      {
         p->list_size = 20;
			p->nodes[n] = (MNode *) malloc(sizeof(MNode));
			p->nodes[n]->next = NULL;
		}
	} else {
      ml_destroy(p);
      return NULL;
   }
	return p;
}

/* ml_add - adds a new MEntry to the list;
 * returns 1 if successful, 0 if error (malloc)
 * returns 1 if it is a duplicate */
int ml_add(MList **ml, MEntry *me) 
{
	MList * p;
   p = *ml;
	unsigned long val = 0;
	int i = 0;
	MNode *node,*new_node;
	if (ml_lookup(p, me) != NULL) return 1;
	if((new_node = (MNode *) malloc(sizeof(MNode)))==NULL)
   {  
      free(new_node);
		return 0;
   }
	new_node->next = NULL;
	val = me_hash(me,p->list_size);
	node = p->nodes[val];
	while(node->next!=NULL) node = node->next;
	node->next = new_node;
	node->me = me;
	return 1;	
}

/* ml_lookup - looks for MEntry in the list, returns matching entry or NULL */
MEntry *ml_lookup(MList *ml, MEntry *me) 
{
	unsigned long val = 0;
	MNode *p;
	int list_size = ml->list_size;
	val = me_hash(me,ml->list_size);
   p = ml->nodes[val];
	while(p->next!=NULL)
   {
		if(me_compare(p->me,me)==0) return p->me;
      else p = p->next;
	}
	ml->list_size = list_size;
	return NULL;
}

/* ml_destroy - iteratively destroy mailing list */
void ml_destroy(MList *ml) 
{
	int i;
	MNode *p, *q;
   for(i=0;i<ml->list_size;i++)
      {
		p = ml->nodes[i];
		while(p->next!=NULL)
      {
			q = p->next;
			me_destroy(p->me);
			free(p);
			p = q;
		}
		free(p);
	}
	free(ml->nodes);
	free(ml);
}


