/*********************************************
 *	Adam Lindsey (alindsey)
 *	CIS425 - Project 2
 *	networkdriver.c
 *	-->this is my own personal work that uses the
 *    instructor provided compiled files
 *********************************************/

/* libraries*/
#include <pthread.h>
#include <unistd.h> 
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "BoundedBuffer.h"
#include "destination.h"
#include "freepacketdescriptorstore.h"
#include "freepacketdescriptorstore__full.h"
#include "networkdevice.h"
#include "packetdescriptor.h"
#include "packetdescriptorcreator.h"
#include "pid.h"

/*global variables required by driver routines*/
NetworkDevice *net_device;
BoundedBuffer **in;
BoundedBuffer *out;
BoundedBuffer *buf;
FreePacketDescriptorStore *my_fpds;

/* definition[s] of functions required for thread[s] */
int expon(int x, int y)
{
   /* local variables */
   int i,n=x;
   /* function body */
   if(y==0) return 1;
   for(i=0;i<y;i++) {
      n*=x;
   } return n;
}/* end expon */

/* Thread Functions */
/* Thread Getter */
void *pthread_getter()
{
   /* local variables */
	PacketDescriptor *pd1, *pd2;
   /* initialize getter */
	blocking_get_pd(my_fpds, &pd1); 
	blocking_get_pd(my_fpds, &pd2);
	init_packet_descriptor(pd1);		
	register_receiving_packetdescriptor(net_device, pd1);
	await_incoming_packet(net_device);
   /* main getter loop */
	while (1)
   {
		if (nonblockingWriteBB(buf, pd1)==1){
			if(nonblocking_get_pd(my_fpds, &pd1)==0){
				init_packet_descriptor(pd2);
				register_receiving_packetdescriptor(net_device, pd2);
				/* inner control loop */
            while(nonblocking_get_pd(my_fpds, &pd1)==0)
            {
					await_incoming_packet(net_device); 
				}/* end while */
			}/* end if */
			init_packet_descriptor(pd1);
			register_receiving_packetdescriptor(net_device, pd1);
			await_incoming_packet(net_device);
		}/* end if */
	}/* end while */
	return NULL;
}/* end pthread_getter() */

/* Thread Setter */
void *pthread_setter()
{
   /* Local Variables */
	int i,n,sleep;
	PacketDescriptor *pd;
   /* main setter loop */
	while (1)
   {
		pd = blockingReadBB(out);
		for (i=0;i<10;i++) {
			if (send_packet(net_device, pd) == 1)break;			
			n=random();
			sleep = (unsigned int) n;
			sleep = sleep % expon(2,i);
			usleep(sleep);	
		}/* end for */
		blocking_put_pd(my_fpds, pd);
	}/* end while */
	return NULL;
}/* end pthread_setter() */

/* pthread buffer */
void *pthread_buffer()
{
   /* Local Variables */
	PID pid;
	PacketDescriptor *pd;
   /* main buffer loop */
	while(1)
   {
		pd = blockingReadBB(buf);
		pid = packet_descriptor_get_pid(pd);
		blockingWriteBB(in[pid], pd);
	}/* end while */
	return NULL;
}/*end pthread_buffer()*/

void init_network_driver(NetworkDevice *nd, void *mem_start, 
         unsigned long mem_length, FreePacketDescriptorStore **fpds_ptr)
{
	int n;
   pthread_t getter, setter, buffer;
   /* create Free Packet Descriptor Store */
   *fpds_ptr = create_fpds();
	my_fpds = *fpds_ptr;
	net_device = nd;
   /* load FPDS with packet from mem_start/mem_length */
   create_free_packet_descriptors(my_fpds, mem_start, mem_length);
   /* create any buffers required by your threads */
   in = malloc(sizeof(in)*(MAX_PID+1)); 
	for (n = 0; n<=MAX_PID;n++) {in[n] = createBB(MAX_PID);}
	out = createBB(MAX_PID);
	buf = createBB(MAX_PID);
   /* create any threads required for implementation */
	pthread_create(&getter, NULL, pthread_getter, NULL);
	pthread_create(&setter, NULL, pthread_setter, NULL);
	pthread_create(&buffer, NULL, pthread_buffer, NULL);
   /* return the FPDS to the code that called */
}/* end init_network_driver */

void blocking_send_packet(PacketDescriptor *pd)
{
   /* queue up packet pd descriptor for sending */
   /* do not return until successfully in queue */
	blockingWriteBB(out, pd);
	return;
}

int  nonblocking_send_packet(PacketDescriptor *pd)
{
   /* if packet is able to queue, do so and return 1 */
   /* otherwise return 0 */
	return nonblockingWriteBB(out, pd);
}

void blocking_get_packet(PacketDescriptor** pd, PID pid)
{
   /* wait until there is a packet for 'pid' */
   /* then return that packet descriptor */
	*pd = blockingReadBB(in[pid]);
	return;
}

int  nonblocking_get_packet(PacketDescriptor** pd, PID pid)
{
   /* if there is currently a waiting packet for 'pid', return it */
   /* to the calling application and have function return 1, else */
   /* return 0 for the value of the function */
	return nonblockingReadBB(in[pid], pd);
}
