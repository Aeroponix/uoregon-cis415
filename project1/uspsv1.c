/*
 * NAME:    Adam Lindsey
 * LOGIN:   alindsey
 * TITLE:   CIS415 - Project1
 * FILE:    uspsv1.c
 * --This is my own personal work--
 */

#include <unistd.h>  //fork, join, _exit
#include <stdlib.h>  //exit
#include <stdio.h>   //printf, perror, goodstuff
#include <sys/wait.h>//pid_t
#include <sys/types.h>
#include "p1fxns.h"  //provided helper functions

/* set up error message for incorrect usage*/
static void usage(void) {
   perror("usage: ./uspsv1.c [workload file location]\n");
}

int main(int argc, char *argv[]) {
   FILE *fd;
   if(argc>2) { usage(); return -1; }
   if(argc>1) {
      fd = fopen(argv[1], "r");
      if(fd==NULL) {
         perror("Error opening file!");
         return -1;
      }
   } else fd = stdin;
   
   char line[256] = "";
   while(fgets(line,sizeof(line),stdin)!=NULL) {
      int state,i;
      pid_t pid[argc];
      for(i=0;i<argc;i++) {
         if((pid[i]=fork())<0) {
            perror("Error forking child process: failed!");
            exit(EXIT_FAILURE);
         } else if(pid[i]==0) {
            if(execvp(line, argv)<0) {
               perror("Error in execvp: exec failed!");
               _exit(1);
            }
         } else (void)waitpid(pid[i], &state, 0);
         return EXIT_SUCCESS;
      }
   }   
   //LAUNCH PROGRAM PSEUDOCODE
   //for i in 0..numprograms-1
      //pid[i] = fork();
      //if (pid[i] > 0)
         //prepare argument structure;
         //execvp(program[i], args[i]);
   //for i in 0..numprograms-1
      //wait(pid[i]);
   
   fclose(fd);
   return 0;
}


