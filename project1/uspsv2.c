/*
 * NAME:    Adam Lindsey
 * LOGIN:   alindsey
 * TITLE:   CIS415 - Project1
 * FILE:    uspsv.c
 * --This is my own personal work--
 */

#include <unistd.h>  //fork, join, _exit
#include <stdlib.h>  //exit
#include <stdio.h>   //printf, perror, goodstuff
#include <string.h>
#include <sys/wait.h>//pid_t
#include <sys/types.h>
#include "p1fxns.h"

/* set up error message for incorrect usage*/
static void usage(void) {
   perror("usage: ./uspsv1.c [workload file location]\n");
}

/*global for keeping track of process num*/
static int count = 0;
void get_commands(char *line, char *argv[]) {
   while (*line != '\0') { 
      while (*line=='\n'||*line==' ') { 
         if(*line=='\n')count++;
         *line++= '\0';
      }
      *argv++= line;
      while (*line != '\0' && *line != '\n') line++;
   }
   *argv = '\0';
}

int main(int argc, char *argv[]) {
   if(argc>1) {usage();return -1;}
   int i,state;
   char line[1024];
   //unfortunately I couldn't figure out otherwise
   while(fgets(line,1024,stdin)!=NULL){
      get_commands(line, argv);
   }
   /*
   //LAUNCH PROGRAM PSEUDOCODE:
   //for i in 0..numprograms-1
      //pid[i] = fork();
      //if (pid[i] > 0)
         //prepare argument structure;
         //execvp(program[i], args[i]);
   //for i in 0..numprograms-1
      //wait(pid[i]);
   */
   pid_t pid[argc];
   for(i=0;i<=count;i++) {
      if((pid[i]=fork())<0) {
         perror("Error forking child process: failed!");
         exit(EXIT_FAILURE);
      } else if(pid[i]==0) {
         if(execvp(*argv, argv)<0) {
            perror("Error in execvp: exec failed!");
            _exit(1);
         }
      } else (void)waitpid(pid[i], &state, 0);
   }
      return 0;
}


